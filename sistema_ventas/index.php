<?php
 include("conexion/miconexion.php");
 include("modelo/usuario.php");
 if ((isset($_POST['rut'])) && ($_POST['rut'] != '') && (isset($_POST['clave'])) && ($_POST['clave'] != ''))
 {
    $objeto=new usuarios();
    $objeto->setrut($_POST['rut']);
    $objeto->setclave($_POST['clave']);
    $resultado=$objeto->Valida_Usuario();
    if($resultado == true)
	{	
        $Adc=$_POST['rut'];
        $tp_usuario=$objeto->TipoUsuario();
        if($tp_usuario == 1)
		{	
            
            echo "<script> alert('Bienvenido Usuario Vendedor'); window.location='modulos/home_vendedor.php';</script>";   
		}
		else 
		{
            
            echo "<script> alert('Bienvenido Usuario ADMINISTRADOR'); window.location='modulos/home.php'</script>";   
		}
    }
    else
    {
        echo "<script> alert('Usuario o clave son incorrectos'); window.location='index.php'</script>";   
    }

}
?>
<!DOCTYPE html>
		<html>
		<head>
		    <title>Inicio De Sesion</title>
		    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
			<!-- vinculo a bootstrap -->
               <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <!-- Temas-->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
            <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
            <link rel="stylesheet" type="text/css" href="css/EstiloIndex.css">
            
            <!--Jquery-->
            <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
            <script type="text/javascript" src="js/jquery.mask.js"></script>
            <script>
                $(document).ready(function($)
                {
                    $('#rut').mask("00.000.000-0");
                });
            </script>  
         
		</head>
		<body>
		 <div id="Contenedor" class="text-center" >
		 <div class="Icon">
                    <!--Icono de usuario-->
                    <img src="img/logo.png" width="150px" height="150px">
        </div>
        <div class="titulo">
                <h1>Sistema de Ventas</h1>
        </div>
		<div class="ContentForm">
		 	<form action="" method="post" name="FormEntrar">
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control" name="rut" placeholder="Ingrese su Rut" id="rut" aria-describedby="sizing-addon1"  required>
				</div>
				<br>
				<div class="input-group input-group-lg">
				  <input  type="password" name="clave" class="form-control" placeholder="Ingrese su Clave" aria-describedby="sizing-addon1" maxlength = "4" required>
				</div>
				<br>
				<button class="btn btn-lg btn-primary  btn-signin" id="IngresoLog" width="80px" height="80px" type="submit">Ingresar</button>
		 	</form>
		 </div>	
		 </div>
</body>
 <!-- vinculando a libreria Jquery-->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <!-- Libreria java scritp de bootstrap -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</html>