function validarUsuario()
{
    var rut,nombre, apellido, clave, expresion;
    nombre = document.getElementById("nombre").value;
    apellido = document.getElementById("apellido").value;
    clave = document.getElementById("clave").value;

    expresion = /^[A-Z]/;
    if(rut === "" ||nombre === "" || apellido === "" || clave === "")
    {
        alert("Debes rellenar todos los campos...");
        return false;
    }

    else if(!expresion.test(nombre))
    {
        alert("Solo debes ingresar letras en mayúsculas");
    }

    else if(!expresion.test(apellido))
    {
        alert("Solo debes ingresar letras en mayúsculas");
    }

    else if (isNaN (clave))
    {
        alert("la clave solo deben ser digitos numericos...")
    }

}


function validarProducto()
{
    var nombre, descripcion, precio, stock, expresion;
    nombre = document.getElementById("nombre").value;
    descripcion = document.getElementById("descripcion").value;
    precio = document.getElementById("precio").value;
    stock = document.getElementById("stock").value;

    expresion = /^[A-Z]/;

    if(nombre === "" ||descripcion === "" || precio === "" || stock === "")
    {
        alert("Debes rellenar todos los campos...");
        return false;
    }

    else if(!expresion.test(nombre))
    {
        alert("Solo debes ingresar letras en mayúsculas");
    }

    else if(!expresion.test(descripcion))
    {
        alert("Solo debes ingresar letras en mayúsculas");
    }

    else if (isNaN (precio))
    {
        alert("El precio solo deben ser digitos numericos...")
    }

    else if (isNaN (stock))
    {
        alert("El stock solo deben ser digitos numericos...")
    }

}