<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Restauracion y Recuperacion</title>
	<link rel="stylesheet" type="text/css" href="../../css/estilobackup.css">
</head>
<body>
	<div id="Contenedor" align="center">
		<h3 id="titulo">Módulo de Restauración y Recuperación</h3>
		<br>
		<a href="Backup.php"><img title="Realizar Copia de seguridad" src="../../img/restore.png" width="100" height="100"></a>
		<form action="Restore.php" method="POST">
			<br>
			<label>Selecciona un punto de restauración</label><br>
			<select name="restorePoint" id="combo">
				<option value="" disabled="" selected="">Selecciona un punto de restauración</option>
				<?php
					include_once './Connet.php';
					$ruta=BACKUP_PATH;
					if(is_dir($ruta)){
						if($aux=opendir($ruta)){
							while(($archivo = readdir($aux)) !== false){
								if($archivo!="."&&$archivo!=".."){
									$nombrearchivo=str_replace(".sql", "", $archivo);
									$nombrearchivo=str_replace("-", ":", $nombrearchivo);
									$ruta_completa=$ruta.$archivo;
									if(is_dir($ruta_completa)){
									}else{
										echo '<option value="'.$ruta_completa.'">'.$nombrearchivo.'</option>';
									}
								}
							}
							closedir($aux);
						}
					}else{
						echo $ruta." No es ruta válida";
					}
				?>
			</select>
			<button id="restorebtn"><img title="Restaurar" src="../../img/recovery.png" width="80" height="80" type="submit"></button>
			<a href="../../modulos/home.php"><img title="Volver" src="../../img/back.png" width="80" height="80"></a>
			<br>
		</form>	
		<br>
		
	</div>
</body>
</html>
