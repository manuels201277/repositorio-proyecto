<?php
    class ventas{
        private $venta;
        private $bd;

        private $id_venta;
        private $id_producto;
        private $nombre_cliente;
        private $apellido_cliente;
        private $cantidad;
        private $total;

        public function __construct()
		{
			$this->bd=miconexion::conexion();
			$this->venta=array();
		}


        public function get_venta()
		{
			$consulta=$this->bd->query("SELECT * FROM venta");
			while($fila=$consulta->fetch_assoc())
			{
			$this->venta[]=$fila;
			}
			return $this->venta;
        }
		
		public function setidventa($id_venta)
		{
			$this->id_venta=$id_venta;
        }
        public function getidventa()
		{
			return $this->id_venta;
		}	

		public function setidproducto($id_producto)
		{
			$this->id_producto=$id_producto;
        }
        public function getidproducto()
		{
			return $this->id_producto;
		}	


        public function setnombrecliente($nombre_cliente)
		{
			$this->nombre_cliente=$nombre_cliente;
        }
        public function getnombrecliente()
		{
			return $this->nombre_cliente;
        }

        public function setapellidocliente($apellido_cliente)
		{
			$this->apellido_cliente=$apellido_cliente;
        }
        public function getapellidocliente()
		{
			return $this->apellido_cliente;
        }
        
        public function setcantidad($cantidad)
		{
			$this->cantidad=$cantidad;
        }
        public function getcantidad()
		{
			return $this->cantidad;
        }

        public function settotal($total)
		{
			$this->total=$total;
        }
        public function gettotal()
		{
			return $this->total;
		}
		


		public function insertar()
		{
			$consulta="INSERT INTO `venta`(`ID_PRODUC`, `NOMBRE_CLIENTE`, `APELLIDO_CLIENTE`, `CANTIDAD`, `TOTAL`) 
			VALUES ($this->id_producto,'$this->nombre_cliente','$this->apellido_cliente',$this->cantidad,$this->total)";
			$resultado=$this->bd->query($consulta);
			if($resultado==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
			
		public function modificar()
		{
			$consulta="UPDATE `venta` SET `ID_PRODUC`=$this->id_producto,`NOMBRE_CLIENTE`= '$this->apellido',`APELLIDO_CLIENTE`= '$this->apellido' ,`CANTIDAD`= $this->cantidad ,`TOTAL`= $this->total WHERE ID_VENTA = $this->id_venta;";
			$resultado=$this->bd->query($consulta);
			if($resultado==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function eliminar()
		{
			$consulta="DELETE FROM `venta` WHERE `ID_VENTA` = $this->id_venta";
			$resultado=$this->bd->query($consulta);
			if($resultado==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function BUSCAR_X_ID()
		{
			$CONSULTA= "SELECT * FROM `producto` WHERE STOCK > 0";
			$RESULTADO= $this->bd->query($CONSULTA);
			return $RESULTADO;
		}

		
		public function CARGAR_PRECIO()
		{
			$CONSULTA= "SELECT `PRECIO` FROM `producto` WHERE `ID_PRODUCTO` = $this->id_producto ";
			$resultado=$this->bd->query($CONSULTA);
			while($fila=$resultado->fetch_assoc()){
				$PP=$fila['PRECIO'];
			}		
			return $PP;
		}
		public function CARGAR_STOCK()
		{
			$CONSULTA= "SELECT `STOCK` FROM `producto` WHERE `ID_PRODUCTO` = $this->id_producto ";
			$resultado=$this->bd->query($CONSULTA);
			while($fila=$resultado->fetch_assoc()){
				$PP=$fila['STOCK'];
			}		
			return $PP;
		}
		public function CARGAR_NOMBRE_PRODUCTO()
		{
			$CONSULTA= "SELECT `NOMBRE_PRODUCTO` FROM `producto` WHERE `ID_PRODUCTO` = $this->id_producto ";
			$resultado=$this->bd->query($CONSULTA);
			while($fila=$resultado->fetch_assoc()){
				$am=$fila['NOMBRE_PRODUCTO'];
			}		
			return $am;
		}


		public function CARGAR_ID_PRODUCTO()
		{
			$CONSULTA= "SELECT NOMBRE_PRODUCTO FROM PRODUCTO";
			$RESULTADO=$this->bd->query($CONSULTA);
			return $RESULTADO;
		}

		public function Relleno_datos()
		{
			$consulta="SELECT * FROM VENTA;";
			$resultado=$this->bd->query($consulta);
			while($fila=$resultado->fetch_assoc()){
				$this->venta[]=$fila;
			}
			return $this->venta;
		} 

		public function Relleno_datos_tabla()
		{
			$consulta="SELECT ID_VENTA, ID_PRODUC, PRODUCTO.NOMBRE_PRODUCTO, PRODUCTO.DESCRIPCION,NOMBRE_CLIENTE,APELLIDO_CLIENTE,CANTIDAD,STOCK,TOTAL
			FROM VENTA
			INNER JOIN PRODUCTO ON VENTA.ID_PRODUC = PRODUCTO.ID_PRODUCTO;";
			$resultado=$this->bd->query($consulta);
			while($fila=$resultado->fetch_assoc()){
				$this->venta[]=$fila;
			}
			return $this->venta;
		} 

    }
?>