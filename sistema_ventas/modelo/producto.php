<?php
	class productos
	{
        private $producto;
        private $bd;

        private $id_producto;
        private $nombre_producto;
        private $descripcion;
        private $stock;
        private $precio;

        public function __construct()
		{
			$this->bd=miconexion::conexion();
			$this->producto=array();
		}


        public function get_producto()
		{
			$consulta=$this->bd->query("SELECT * FROM producto");
			while($fila=$consulta->fetch_assoc())
			{
			$this->producto[]=$fila;
			}
			return $this->producto;
        }
        
		public function setidproducto($id_producto)
		{
			$this->id_producto=$id_producto;
        }
        public function getidproducto()
		{
			return $this->id_producto;
		}	


        public function setnombreproducto($nombre_producto)
		{
			$this->nombre_producto=$nombre_producto;
        }
        public function getnombreproducto()
		{
			return $this->nombre_producto;
        }

        public function setdescripcion($descripcion)
		{
			$this->descripcion=$descripcion;
        }
        public function getdescripcion()
		{
			return $this->descripcion;
        }
        
        public function setstock($stock)
		{
			$this->stock=$stock;
        }
        public function getstock()
		{
			return $this->stock;
        }

        public function setprecio($precio)
		{
			$this->precio=$precio;
        }
        public function getprecio()
		{
			return $this->precio;
		}
		/*******************************************METODOS DE CRUD*******************************************************/
	
		public function INSERTAR()
		{
			$CONSULTA = "	INSERT INTO `producto`( `NOMBRE_PRODUCTO`, `DESCRIPCION`, `STOCK`, `PRECIO`) VALUES 
			('$this->nombre_producto','$this->descripcion',$this->stock,$this->precio)";
			$RESULTADO= $this->bd->query($CONSULTA);
			if($RESULTADO==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		public function MODIFICAR()
		{
			$CONSULTA="UPDATE `producto` SET `NOMBRE_PRODUCTO`='$this->nombre_producto',
			`DESCRIPCION`='$this->descripcion',`STOCK`=$this->stock,`PRECIO`=$this->precio
			WHERE `ID_PRODUCTO`= $this->id_producto "; 
			$RESULTADO=$this->bd->query($CONSULTA);
			if($RESULTADO==true) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function ELIMINAR()
		{
			$CONSULTA="DELETE  FROM `producto` WHERE `ID_PRODUCTO` = '$this->id_producto'"; 
			echo $CONSULTA;
			$RESULTADO=$this->bd->query($CONSULTA);
			if($RESULTADO==true) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		public function MODIFICAR_STOCK()
		{
			$CONSULTA="UPDATE `producto` SET `STOCK`=$this->stock
			WHERE `ID_PRODUCTO`= $this->id_producto "; 
			$RESULTADO=$this->bd->query($CONSULTA);
			if($RESULTADO==true) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}



		/*******************************************METODOS DE BUSQUEDA*******************************************************/
		public function Comprobar_Codigo()
		{
			$CONSULTA= "SELECT `ID_PRODUCTO` FROM `producto` WHERE `ID_PRODUCTO` = $this->id_producto";
			$RESULTADO= $this->bd->query($CONSULTA);
			while($fila=$RESULTADO->fetch_assoc())
			{
				$this->producto[]=$fila;
			}
			return $this->producto;
		}
		public function Comprobar_Nombre()
		{
			$CONSULTA= "SELECT `NOMBRE_PRODUCTO` FROM `producto` WHERE `NOMBRE_PRODUCTO` = '$this->nombre_producto'";
			$RESULTADO= $this->bd->query($CONSULTA);
			while($fila=$RESULTADO->fetch_assoc())
			{
				$this->producto[]=$fila;
			}
			return $this->producto;
		}
		public function BUSCAR_X_CODIGO()
		{
			$CONSULTA="SELECT `ID_PRODUCTO`, `NOMBRE_PRODUCTO`, `DESCRIPCION`, `STOCK`, `PRECIO` 
			FROM `producto` WHERE `ID_PRODUCTO` = $this->id_producto";
			$RESULTADO=$this->bd->query($CONSULTA);
			while($fila=$RESULTADO->fetch_assoc()){
				$this->producto[]=$fila;
			}
			return $this->producto;
		}

		public function CARGAR_PRODUCTOS()
		{
			$CONSULTA="SELECT * FROM `producto`";
			$RESULTADO=$this->bd->query($CONSULTA);
			return $RESULTADO;
		}

		
		public function Relleno_datos_tabla()
		{
			$consulta="SELECT * FROM `producto`";
			$resultado=$this->bd->query($consulta);
			while($fila=$resultado->fetch_assoc()){
				$this->producto[]=$fila;
			}
			return $this->producto;
		} 
    }
?>