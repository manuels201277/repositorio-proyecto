<?php
     class usuarios{
        private $usuario;
        private $bd;

        private $rut;
        private $nombre;
        private $apellido;
		private $clave;
		private $tipo;

        public function __construct()
		{
			$this->bd=miconexion::conexion();
			$this->usuario=array();
		}


        public function get_rut_usuario()
		{
			$consulta=$this->bd->query("SELECT * FROM usuario");
			while($fila=$consulta->fetch_assoc())
			{
			$this->usuario[]=$fila;
			}
			return $this->usuario;
        }
        
		public function setrut($rut)
		{
			$this->rut=$rut;
        }
        public function getrut()
		{
			return $this->rut;
		}	


        public function setnombre($nombre)
		{
			$this->nombre=$nombre;
        }
        public function getnombre()
		{
			return $this->nombre;
        }

        public function setapellido($apellido)
		{
			$this->apellido=$apellido;
        }
        public function getapellido()
		{
			return $this->apellido;
        }
        
        public function setclave($clave)
		{
			$this->clave=$clave;
        }
        public function getclave()
		{
			return $this->clave;
		}


		public function settipo($tipo)
		{
			$this->tipo=$tipo;
        }
        public function gettipo()
		{
			return $this->tipo;
		}
		
		/*-------------------------Validacion de Usuario---------------------------------------------------*/
		public function Valida_Usuario()
		{
			$consulta="SELECT * FROM `usuario` WHERE `RUT`= '$this->rut' AND `CLAVE` = $this->clave;";
			$resultado=$this->bd->query($consulta);
			if($resultado->num_rows>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		} 

		public function insertar()
		{
			$consulta="INSERT INTO `usuario`(`RUT`,`NOMBRE`,`APELLIDO`, `CLAVE`, `TIPO_USUARIO`) VALUES('$this->rut','$this->nombre','$this->apellido' ,$this->clave, $this->tipo)";
			$resultado=$this->bd->query($consulta);
			if($resultado==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
			
		public function modificar()
		{
			$consulta="UPDATE `usuario` SET `NOMBRE`='$this->nombre',`APELLIDO`= '$this->apellido' ,
			`CLAVE`= $this->clave ,`TIPO_USUARIO`= $this->tipo WHERE RUT = '$this->rut';";
			$resultado=$this->bd->query($consulta);
			if($resultado==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function eliminar()
		{
			$consulta="DELETE FROM `usuario` WHERE `RUT` = '$this->rut'";
			$resultado=$this->bd->query($consulta);
			if($resultado==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function BUSCAR_X_RUT()
		{
			$CONSULTA="SELECT `RUT`, `NOMBRE`, `APELLIDO`, `CLAVE`, `TIPO_USUARIO`, `TIPO` FROM
			 `usuario` INNER JOIN tipo_usuario ON usuario.TIPO_USUARIO= tipo_usuario.ID WHERE `RUT`= '$this->rut'";
			$RESULTADO=$this->bd->query($CONSULTA);
			while($fila=$RESULTADO->fetch_assoc()){
				$this->usuario[]=$fila;
			}
			return $this->usuario;
		}

		public function Comprobar_Rut()
		{
			$CONSULTA= "SELECT `RUT` FROM `usuario` WHERE `RUT` = '$this->rut'";
			$RESULTADO= $this->bd->query($CONSULTA);
			while($fila=$RESULTADO->fetch_assoc())
			{
				$this->usuario[]=$fila;
			}
			return $this->usuario;
		}

		public function CARGAR_USUARIOS()
		{
			$CONSULTA="SELECT * FROM `usuario`";
			$RESULTADO=$this->bd->query($CONSULTA);
			return $RESULTADO;
		}

	    public function TipoUsuario()
		{
			$consulta="select TIPO_USUARIO from usuario where RUT='$this->rut' and clave= $this->clave;";
			$resultado=$this->bd->query($consulta);
			while ($fila=$resultado->fetch_assoc()) 
			{
				$al=$fila['TIPO_USUARIO'];
			}
			return $al;
			}	

		public function Relleno_datos_tabla()
		{
			$consulta="SELECT * FROM `usuario`";
			$resultado=$this->bd->query($consulta);
			while($fila=$resultado->fetch_assoc()){
				$this->usuario[]=$fila;
			}
			return $this->usuario;
		} 

    }
?>