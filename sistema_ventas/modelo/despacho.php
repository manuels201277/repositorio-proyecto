<?php
    class despachos{
        private $despacho;
        private $bd;

        private $id_despacho;
        private $id_venta;
        private $origen;
        private $destino;
		private $estado;
		private $direccion;

        public function __construct()
		{
			$this->bd=miconexion::conexion();
			$this->despacho=array();
		}


        public function get_despacho()
		{
			$consulta=$this->bd->query("SELECT * FROM despacho");
			while($fila=$consulta->fetch_assoc())
			{
			$this->despacho[]=$fila;
			}
			return $this->despacho;
        }
        
        public function setiddespacho($id_despacho)
		{
			$this->id_despacho=$id_despacho;
        }
        public function getiddespacho()
		{
			return $this->id_despacho;
		}	

		public function setidventa($id_venta)
		{
			$this->id_venta=$id_venta;
        }
        public function getidventa()
		{
			return $this->id_venta;
		}	


        public function setorigen($origen)
		{
			$this->origen=$origen;
        }
        public function getorigen()
		{
			return $this->origen;
        }

        public function setdestino($destino)
		{
			$this->destino=$destino;
        }
        public function getdestino()
		{
			return $this->destino;
		}
		
		public function setdireccion($direccion)
		{
			$this->direccion=$direccion;
        }
        public function getdireccion()
		{
			return $this->direccion;
        }
        
        public function setestado($estado)
		{
			$this->estado=$estado;
        }
        public function getestado()
		{
			return $this->estado;
		}

		/********************************* CRUD************************************ */
		public function INSERTAR()
		{
			$CONSULTA = "INSERT INTO `despacho`(`ID_VENT`, `ORIGEN`, `DESTINO`, `DIRECCION`, `ESTADO_ACTUAL`) VALUES 
			($this->id_venta,'$this->origen','$this->destino','$this->direccion',1)";
			$RESULTADO= $this->bd->query($CONSULTA);
			if($RESULTADO==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		public function MODIFICAR()
		{
			$CONSULTA = "UPDATE `despacho` SET `ESTADO_ACTUAL`=$this->estado WHERE `ID_DESPACHO`= $this->id_despacho";
			$RESULTADO= $this->bd->query($CONSULTA);
			if($RESULTADO==true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	/****************************VALIDADORES Y OTROS ********************************** */	
		public function Relleno_datos()
		{
			$consulta="SELECT * FROM DESPACHO;";
			$resultado=$this->bd->query($consulta);
			while($fila=$resultado->fetch_assoc()){
				$this->despacho[]=$fila;
			}
			return $this->despacho;
		} 
		
		public function relleno_datos_tabla()
		{
			$CONSULTA="SELECT ID_DESPACHO, ID_VENT,PRODUCTO.NOMBRE_PRODUCTO,VENTA.CANTIDAD,VENTA.NOMBRE_CLIENTE,VENTA.APELLIDO_CLIENTE, ORIGEN, DESTINO, DIRECCION, TIPO_DESPACHO.ESTADO 
			FROM DESPACHO 
			INNER JOIN VENTA ON DESPACHO.ID_VENT = VENTA.ID_VENTA 
			INNER JOIN TIPO_DESPACHO ON TIPO_DESPACHO.ID_ESTADO = DESPACHO.ESTADO_ACTUAL 
			INNER JOIN PRODUCTO ON VENTA.ID_PRODUC = PRODUCTO.ID_PRODUCTO;";
			$RESULTADO=$this->bd->query($CONSULTA);
			return $RESULTADO;
		}
		public function CARGAR_PRODUCTOS()
		{
			$CONSULTA="SELECT `ID_VENTA`, `NOMBRE_CLIENTE`,`APELLIDO_CLIENTE`, `NOMBRE_PRODUCTO`,`CANTIDAD`
			FROM `venta` 
		   INNER JOIN producto ON producto.ID_PRODUCTO=venta.ID_PRODUC 
		   LEFT JOIN despacho ON despacho.ID_VENT = venta.ID_VENTA
		   WHERE despacho.ID_VENT Is NULL";
			$RESULTADO=$this->bd->query($CONSULTA);
			return $RESULTADO;
		}
		public function CARGAR_COMBO_MODIFICACION()
		{
			$CONSULTA="SELECT `ID_VENTA`, `NOMBRE_CLIENTE`,`APELLIDO_CLIENTE`, `NOMBRE_PRODUCTO`,`CANTIDAD`,despacho.ID_DESPACHO, despacho.ESTADO_ACTUAL
			FROM `venta` 
			INNER JOIN producto ON producto.ID_PRODUCTO=venta.ID_PRODUC 
			INNER JOIN despacho ON despacho.ID_VENT = venta.ID_VENTA
			GROUP BY despacho.ID_DESPACHO";
			$RESULTADO=$this->bd->query($CONSULTA);
			return $RESULTADO;
		}
		public function BUSCAR_X_CODIGO()
		{
			$CONSULTA="SELECT `ID_VENTA`, `NOMBRE_CLIENTE`,`APELLIDO_CLIENTE`, `NOMBRE_PRODUCTO`,
			`CANTIDAD`,despacho.ID_DESPACHO,despacho.ESTADO_ACTUAL,despacho.ORIGEN,despacho.DESTINO,
			 despacho.DIRECCION,tipo_despacho.ESTADO 
			FROM `venta` 
			INNER JOIN producto ON producto.ID_PRODUCTO=venta.ID_PRODUC 
			INNER JOIN despacho ON despacho.ID_VENT = venta.ID_VENTA 
			INNER JOIN tipo_despacho on despacho.ESTADO_ACTUAL= tipo_despacho.ID_ESTADO 
			WHERE despacho.ID_DESPACHO=  $this->id_despacho";
			$RESULTADO=$this->bd->query($CONSULTA);
			while($fila=$RESULTADO->fetch_assoc()){
				$this->despacho[]=$fila;
			}
			return $this->despacho;
		}


    }
?>