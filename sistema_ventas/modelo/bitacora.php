<?php
     class bitacoras{
        private $bitacora;
        private $bd;

        private $id_bitacora;
        private $id_persona;
        private $fecha;
		private $ejecutor;
        private $actividad;
        private $info_actual;
        private $info_anterior;

        public function __construct()
		{
			$this->bd=miconexion::conexion();
			$this->bitacora=array();
		}


        public function get_id_bitacora()
		{
			$consulta=$this->bd->query("SELECT * FROM bitacora");
			while($fila=$consulta->fetch_assoc())
			{
			$this->bitacora[]=$fila;
			}
			return $this->bitacora;
        }
        
		public function set_idbitacora($id_bitacora)
		{
			$this->id_bitacora=$id_bitacora;
        }
        public function get_idbitacora()
		{
			return $this->id_bitacora;
		}	


        public function set_id_persona($id_persona)
		{
			$this->id_persona=$id_persona;
        }
        public function get_id_persona()
		{
			return $this->id_persona;
        }

        public function set_fecha($fecha)
		{
			$this->fecha=$fecha;
        }
        public function get_fecha()
		{
			return $this->fecha;
        }
        
        public function set_ejecutor($ejecutor)
		{
			$this->ejecutor=$ejecutor;
        }
        public function get_ejecutor()
		{
			return $this->ejecutor;
		}


		public function set_actividad($actividad)
		{
			$this->actividad=$actividad;
        }
        public function get_actividad()
		{
			return $this->actividad;
		}
        
        public function set_info_actual($info_actual)
		{
			$this->info_actual=$info_actual;
        }
        public function get_info_actual()
		{
			return $this->info_actual;
        }
        

        public function set_info_anterior($info_anterior)
		{
			$this->info_anterior=$info_anterior;
        }
        public function get_info_anterior()
		{
			return $this->info_anterior;
		}

		public function Relleno_datos()
		{
			$consulta="SELECT * FROM BITACORA;";
			$resultado=$this->bd->query($consulta);
			while($fila=$resultado->fetch_assoc()){
				$this->bitacora[]=$fila;
			}
			return $this->bitacora;
		} 
	 }
?>