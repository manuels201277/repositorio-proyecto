<?php
 $Despachos = new despachos();
 if((isset($_POST['id1'])) && ($_POST['id1'] != ''))
{
    $Despachos->setiddespacho($_POST ['id1']);
    $Rest= $Despachos->BUSCAR_X_CODIGO();
}
?>
<?php
    if((isset($_POST['id2'])) && ($_POST['id2'] != '')&&
    (isset($_POST['numeroestado'])) && ($_POST['numeroestado'] != ''))
    {
           
        $Despachos->setiddespacho($_POST ['id2']);  
		$Despachos->setestado($_POST ['numeroestado']); 			
		$consulta = $Despachos->MODIFICAR();
		if($consulta==true)
		{
			echo "<script> alert('Despacho Modificado Correctamente');</script>";    
					
		}
		else
		{
			
			echo "<script> alert('Ocurrio Un Error Intente Nuevamente');window.location='../modulos/home.php'</script>";
		}

    }
    else
    {

    }

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Modificar Estado de Despacho</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
        <link rel="stylesheet" type="text/css" href="../css/EstiloDespacho.css">
	</head>
	<body>
        <div id="Contenedor" class="center">
		 	<div class="Icon">
                <!--Icono de usuario-->
                <img src="../img/product.png" width="50px" height="50px"/>
            </div>
			<div class="ContentForm ">
                <form  class="text-center" action="" method="post" name="FormEntrar">
                    <label for="inputAddress">SELECCIONE EL DESPACHO</label>
                    <div class="input-group input-group-lg center">
                        <select   id="id1" name="id1" class="form-control text-center" > 
                            <option>-----Seleccione-------</option>
                            <?php while ($registro=$resultado->fetch_assoc()) {?>	
                            <option  value="<?php echo $registro['ID_DESPACHO'];?>">
                            <?php echo $registro['CANTIDAD'];?> &nbsp;
                            <?php  echo $registro['NOMBRE_PRODUCTO'];?> &nbsp; 
                            <?php  echo $registro['NOMBRE_CLIENTE']; echo ' '; echo $registro['APELLIDO_CLIENTE']; }?> &nbsp; 
                            </option>
                        </select>
                    </div>
                    <br>
                    <div class=" text-center ">
                        <table align="center">
                        <tr>
                        <td><button class="btn  btn-primary  btn-signin" id="IngresoLog" width="80px" height="80px" type="submit">CARGAR</button></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td> <a class="btn btn-danger" href="../modulos/home.php" role="button">VOLVER</a></td>
                        </tr>
                        </table>  
                    </div>
                </form>
                <br>
                <?php if(isset($_POST['id1'])){  ?>
                    <form action="" method="post" name="FormEntrar2"  style=" text-align:center;">		
                        <?php  foreach ($Rest as $Rest) {?>
                        <label for="inputAddress">Numero de Despacho</label>
                        <div class="input-group input-group-lg">
                        <input type="text" class="form-control text-center" style = "text-transform:uppercase"
                        name="id2" value="<?php echo $Rest['ID_DESPACHO'];?>"
                        aria-describedby="sizing-addon1"  readonly="true">
                        </div>

                        <label for="inputAddress">Nombre Destinatario</label>
                        <div class="input-group input-group-lg">
                        <input type="text" class="form-control text-center" style = "text-transform:uppercase"
                        name="NOMBREYAPELLIDO" placeholder="ej: Queso" 
                        value="<?php echo $Rest['NOMBRE_CLIENTE'];echo' ';echo $Rest['APELLIDO_CLIENTE']?>"
                        aria-describedby="sizing-addon1"  readonly="true">
                        </div>
                        <br>
                        <label for="inputAddress">Cantidad y Producto</label>
                        <div class="input-group input-group-lg">
                        <input type="text" class="form-control text-center" style = "text-transform:uppercase" 
                        name="CANTIDADYNOMBRE" placeholder="ej: Queso" 
                        value="<?php echo $Rest['CANTIDAD'];echo' ';echo $Rest['NOMBRE_PRODUCTO']?>"
                        aria-describedby="sizing-addon1" readonly="true">
                        </div>
                        <br>
                        <label for="inputAddress2">ENVIO ORIGEN</label>
                        <div class="input-group input-group-lg">
                        <input  type="text" name="origen" class="form-control text-center"  style = "text-transform:uppercase"
                        value="<?php echo $Rest['ORIGEN'];?>"
                        aria-describedby="sizing-addon1"  readonly="true">
                        </div>
                        <br>
                        <label for="inputAddress">ENVIO DESTINO</label>
                        <div class="input-group input-group-lg">
                        <input type="text" class="form-control text-center"  name="destino" style = "text-transform:uppercase" 
                        value="<?php echo $Rest['DESTINO'];?>"
                        aria-describedby="sizing-addon1"  readonly="true">
                        </div>
                        <br>
                        <label for="inputAddress2">DIRECCION DE ENVIO</label>
                        <div class="input-group input-group-lg">
                        <input  type="text" name="direccion" class="form-control text-center"  style = "text-transform:uppercase" 
                        value="<?php echo $Rest['DIRECCION'];?>"
                        aria-describedby="sizing-addon1"  readonly="true">
                        </div>
                        <br>
                        <label for="inputAddress">ESTADO DESPACHO</label>
                        <div class="input-group input-group-lg center">
                            <select  id="numeroestado" name="numeroestado"  class="form-control"> 
                            <?php 
                                if($Rest['ESTADO_ACTUAL']==1)
                                {
                                    ?><option  value="<?php echo $Rest['ESTADO_ACTUAL'];?>"><?php echo $Rest['ESTADO'];?></option>
                                    <option  value=2>EN CAMINO</option>
                                    <option  value=3>ENTREGADO</option>
                                    <option  value=4>CANCELADO</option>
                                <?php
                                }
                                else if($Rest['ESTADO_ACTUAL']==2)
                                {?>
                                    <option  value="<?php echo $Rest['ESTADO_ACTUAL'];?>"><?php echo $Rest['ESTADO'];?></option> 
                                    <option  value=1>CONFIRMADO</option>
                                    <option  value=3>ENTREGADO</option>
                                    <option  value=4>CANCELADO</option>
                                <?php
                                }
                                else if($Rest['ESTADO_ACTUAL']==3)
                                {?>
                                    <option  value="<?php echo $Rest['ESTADO_ACTUAL'];?>"><?php echo $Rest['ESTADO'];?></option> 
                                    <option  value=1>CONFIRMADO</option>
                                    <option  value=2>EN CAMINO</option>
                                    <option  value=4>CANCELADO</option>
                                <?php
                                }
                                else 
                                {?>
                                    <option  value="<?php echo $Rest['ESTADO_ACTUAL'];?>"><?php echo $Rest['ESTADO'];?></option> 
                                    <option  value=1>CONFIRMADO</option>
                                    <option  value=2>EN CAMINO</option>
                                    <option  value=3>ENTREGADO</option>
                                <?php
                                }?>
                            </select><br>
                        </div>
                        <?php } ?>
                        <div >
                        <br>
                            <table  align="center"> 
                                <tr>
                                    <td>
                                        <button class="btn  btn-primary  btn-signin " id="IngresoLog" width="80px" 
                                        height="80px" type="submit">ACTUALIZAR</button>
                                    </td>
                                </tr>
                            </table> 
                        </div>              
                    </form>
                <?php } ?>   
		    </div>	
		</div>
    </body>
</html>