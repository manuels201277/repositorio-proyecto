<?php
 include("../conexion/miconexion.php");
 include("../modelo/usuario.php");
 $Usuarios = new usuarios();
	if(
        (isset($_POST['rut'])) && ($_POST['rut'] != '')&&
		(isset($_POST['nombre'])) && ($_POST['nombre'] != '')&&
		(isset($_POST['apellido'])) && ($_POST['apellido'] != '')&&
		(isset($_POST['clave'])) && ($_POST['clave'] != '')&&
		(isset($_POST['tipo'])) && ($_POST['tipo'] != '')
		)
	{  
		$Usuarios->setrut($_POST ['rut']);
	    $respuesta=$Usuarios->Comprobar_Rut();
		if($respuesta==false)
		{
			$Usuarios->setnombre($_POST ['nombre']);  
			$Usuarios->setapellido($_POST ['apellido']); 
			$Usuarios->setclave($_POST ['clave']);
			$Usuarios->settipo($_POST ['tipo']);
			$consulta = $Usuarios->insertar();
			if($consulta==true)
			{
				echo "<script> alert('Usuario Creado correctamente');</script>";    
					
			}
			else
			{
			
				echo "<script> alert('Ocurrio Un Error Intente Nuevamente');window.location='home.php'</script>";
			}
		}
		else
		{	
			echo "<script> alert('EL RUT DE Usuario YA EXISTE, PORFAVOR VERIFICA LA INFORMACION');window.location='home.php'</script>";
		}
	}	
    else
    {
        
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Crear Usuario</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
        <link rel="stylesheet" type="text/css" href="../css/EstiloUsuario.css">
		<script src="../js/validaciones.js"></script>

        <!--Jquery-->
        <script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="../js/jquery.mask.js"></script>
        <script>
            $(document).ready(function($)
            {
                $('#rut').mask("00.000.000-0");
            });
        </script> 
	</head>
	<body>
	<div id="Contenedor" align="center">
		 <div class="Icon">
                    <!--Icono de usuario-->
                   <img src="../img/user.png" width="50px" height="50px"/>
        </div>
        <div class="ContentForm">
		 	<form action="" method="post" name="FormEntrar" onsubmit="return validarUsuario()">
			    <label for="inputAddress">RUN </label>
		 		<div class="input-group input-group-lg">
                 <input type="text" class="form-control" name="rut" style = "text-transform:uppercase" placeholder="Ingrese su Rut" id="rut" aria-describedby="sizing-addon1">
				</div>
				<br>
				<label for="inputAddress2">Nombre</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="nombre" class="form-control" style = "text-transform:uppercase" placeholder="ingrese nombre" id="nombre"  aria-describedby="sizing-addon1">
				</div>
				<br>
                <label for="inputAddress">Apellido</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control" name="apellido" style = "text-transform:uppercase" placeholder="ingrese apellido" id="apellido" aria-describedby="sizing-addon1">
				</div>
                <br>
                <label for="inputAddress">Clave</label>
		 		<div class="input-group input-group-lg">
				  <input type="password" class="form-control" name="clave" placeholder="ingrese una clave" id="clave" aria-describedby="sizing-addon1" maxlength = "4">
				</div>
                <br>
				<label for="inputAddress">TIPO DE USUARIO </label>
				<div class="input-group input-group-lg center">
					<select   id="combo" name="tipo" class="form-control"> 
					<option selected >-- SELECCIONE --</option>
					<option value=1>VENDEDOR</option>
					<option value=2>ADMINISTRADOR</option>
					</select>
				</div>
				<br>
                <button class="btn btn-lg btn-primary  btn-signin" id="IngresoLog" width="80px" height="80px" type="submit">CREAR</button>
		 	</form>
		 	<br>
		 	<a class="btn btn-danger" href="../modulos/home.php" role="button">VOLVER</a>
		 </div>	
	</div>
</body>
</html>


<!--pattern="^[0-9]+" -->