<?php
 include("../Conexion/miconexion.php");
 include("../Modelo/producto.php");
 $Productos = new productos();
	if(
		(isset($_POST['nombre'])) && ($_POST['nombre'] != '')&&
		(isset($_POST['descripcion'])) && ($_POST['descripcion'] != '')&&
		(isset($_POST['stock'])) && ($_POST['stock'] != '')&&
		(isset($_POST['precio'])) && ($_POST['precio'] != '')
		
		)
	{  
		$Productos->setNOMBREPRODUCTO($_POST ['nombre']);
	    $respuesta=$Productos->Comprobar_Nombre();
		if($respuesta==false)
		{
			$Productos->setnombreproducto($_POST ['nombre']);  
			$Productos->setdescripcion($_POST ['descripcion']);
			$Productos->setstock($_POST ['stock']); 	
			$Productos->setprecio($_POST ['precio']);
			$consulta = $Productos->INSERTAR();
			if($consulta==true)
			{
				echo "<script> alert('Producto Creado correctamente'); window.location='home.php'</script>";    
					
			}
			else
			{
			
				echo "<script> alert('Ocurrio Un Error Intente Nuevamente');window.location='home.php'</script>";
			}
		}
		else
		{	
			echo "<script> alert('EL NOMBRE DE PRODUCTO YA EXISTE, PORFAVOR VERIFICA LA INFORMACION');window.location='home.php'</script>";
		}
	}	
    else
    {
        
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Crear Producto</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
        <link rel="stylesheet" type="text/css" href="../css/EstiloIngresar.css">
		<script src="../js/validaciones.js"></script>
	</head>
	<body>
	<div id="Contenedor" align="center">
		 <div class="Icon">
                    <!--Icono de usuario-->
                   <img src="../img/product.png" width="50px" height="50px"/>
        </div>
        <div class="ContentForm">
		 	<form action="" method="post" name="FormEntrar" onsubmit="return validarProducto()">
			    <label for="inputAddress">Nombre del Producto</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control" name="nombre" style = "text-transform:uppercase" placeholder="nombre producto" id="nombre" aria-describedby="sizing-addon1" >
				</div>
				<br>
				<label for="inputAddress2">Descripcion</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="descripcion" class="form-control" style = "text-transform:uppercase" placeholder="ej:color rojo"  aria-describedby="sizing-addon1" >
				</div>
				<br>
                <label for="inputAddress">Stock</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control" name="stock" placeholder="ingrese cantidad" id="sotck" aria-describedby="sizing-addon1" >
				</div>
                <br>
                <label for="inputAddress">Precio</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control" name="precio" placeholder="ingrese precio" id="precio" aria-describedby="sizing-addon1" >
				</div>
                <br>
                <button class="btn btn-lg btn-primary  btn-signin" id="IngresoLog" width="80px" height="80px" type="submit">CREAR</button>
		 	</form>
		 	<br>
		 	<a class="btn btn-danger" href="../modulos/home.php" role="button">VOLVER</a>
		 </div>	
	</div>
</body>
</html>