<!DOCTYPE html>
<html>
<head>
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="">
<style>
table, th, td {
  border: 2px solid black;
  border-collapse: collapse;
  font-family:verdana;
 
}
th, td {
  padding: 5px;
  text-align: center;
  font-family:verdana;
  font-size:12px;
}
</style>
</head>
<body align="center">
    <div id="cont">
        <div id="encabezado" class="text-center">
		<br>
            <h1>Ver todas las ventas</h1>
		<br>
        </div>
        <div id="contenido" class="text-center" >
			<table style="  margin:0 auto; " class="text-center" >
				<tr class="bg-info">
					<td>ID VENTA</td><td>CÓDIGO PRODUCTO</td><td>NOMBRE PRODUCTO</td>
					<td>DESCRIPCION</td><td>CLIENTE</td>
					<td>CANTIDAD</td><td>TOTAL</td>
				</tr>
				<?php
					foreach($datos as $datos)
					{ 
						echo "<tr>
						<td>".$datos['ID_VENTA']."</td>
						<td>".$datos['ID_PRODUC']."</td>
						<td>".$datos['NOMBRE_PRODUCTO']."</td>
						<td>".$datos['DESCRIPCION']."</td>
						<td>".$datos['NOMBRE_CLIENTE']."
						".$datos['APELLIDO_CLIENTE']."</td>
						<td>".$datos['CANTIDAD']."</td>
						<td>".$datos['TOTAL']."</td>
						</tr>"; 
					} 
				?>
			</table>
			
		</div>
		<br>
		<div class="text-center">	
		
			<a href="../modulos/home.php"><img title="Volver" src="../img/back.png" width="60" height="60"></a>
		</div>
    </body>
</html>