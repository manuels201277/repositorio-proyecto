<!DOCTYPE html>
<html>
    <head>
        <title>Crear Venta</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
        <link rel="stylesheet" type="text/css" href="../css/estiloVenta.css">
	</head>
	<body>
	<div id="Contenedor" align="center">
		 <div class="Icon">
                    <!--Icono de usuario-->
					<h2 id="titulo">Boleta Electrónica</h2>
                   <img src="../img/ticket.png" width="70px" height="70px"/>
        </div>
        <div class="ContentForm">
		 	<form action="../modulos.home.php" method="post" name="FormEntrar"  style="text-center">
			 <label for="inputAddress" id="producto">Producto</label>
                <div class="input-group input-group-lg">
				  <input  type="text" name="NUMERO" class="form-control" 
                  style = "text-transform:uppercase" readonly="true"
                  aria-describedby="sizing-addon1" value="<?php echo $NOMBREP ?>">
				</div>
				<br>
				<label for="inputAddress2">Nombre Cliente</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="nombre" class="form-control" style = "text-transform:uppercase" 
                  placeholder="ingrese nombre"  value="<?php echo $Ncliente ?>" aria-describedby="sizing-addon1" readonly="true">
				</div>
				<br>
                <label for="inputAddress">Apellido Cliente</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control"  value="<?php echo $Acliente ?>" name="apellido"
                   style = "text-transform:uppercase" placeholder="ingrese apellido" id="apellido" 
                   aria-describedby="sizing-addon1" readonly="true">
				</div>
                <br>
                <label for="inputAddress">Cantidad</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control"  value="<?php echo $Cproducto ?>" 
                  name="cantidad" placeholder="ingrese cantidad" id="clave" pattern="^[0-9]+" aria-describedby="sizing-addon1" 
                  maxlength = "7" readonly="true">
				</div>
				<br>
				<label for="inputAddress">Total</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control" name="total" placeholder="ingrese total" id="total" 
                  pattern="^[0-9]+" aria-describedby="sizing-addon1" maxlength = "7" value="<?php echo $TOTAL ?>" readonly="true">
				</div>
                <br>
		 	</form>
		 	<a class="btn btn-danger" href="../modulos/home.php" role="button">VOLVER</a>
		 </div>	
	</div>
</body>
</html>


