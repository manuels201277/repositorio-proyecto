
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Centro de Ayuda</title>
        <link rel="stylesheet" type="text/css" href="../css/estiloayuda.css">
    </head>
    <body>
    <div id="cont">
        <div id="encabezado">
            <h1>Cento de ayuda Administrador</h1>
        </div>
        <div id="contenido">
            <h2>Bienvenidos <span>al centro de ayuda para el Administrador</span></h2>
            <p>- Primero que todo, el sistema tiene una funcionalidad en la cual pide autenticación, que consiste en ingresar su RUN y su clave de acceso. 
                para ello, usted solo debe ingresar los digitos; los puntos y el guión lo ingresa el sistema.
            </p>
            <br>
            <img src="../img/login.png" border="1" width="300" height="500">
            <br>
            <p>- Luego de iniciar sesión, el sistema lo redireccionará a su menu correspondinte, el cual usted posee todas las funcionalidades a su disposición en la cual usted podrá 
                crear, modificar y eliminar usuarios; a su vez usted podrá determinar si el usuario a crear será de tipo "Vendedor" o un "Administrador" aunque la recomendación es que solo exista un administrador. El usuario administrador podrá 
                agregar, modificar y eliminar productos, crear ventas y despachos. Ofreciendole el control total al usuario administrador.
            </p>
            <br>
            <img src="../img/menuadmin.png" border="1" width="400" height="200">
            <br>
            <p> - Cada uno de los formularios estan validados con los campos que deben ir, es decir, en los campos donde se ingresan textos como nombre, apellido etc. sólo textos en mayúsculas
                y los campos que sean de tipo numérico, solo se deben ingresar digitos.
            </p>
            <p> Cualquier dato que no cumplan el formato arrojará una alerta indicando cual es el error cometido a la hora 
                de rellenar el formulario.
            </p>
            <br>
            <img src="../img/backup.png" border="1" width="100" height="100">
            <br>
            <p>- En el menu aparece este icono haciendo referencia al módulo de respaldo y restauración del sistema.
                El cual consiste en hacer click en la primera imagen y se realiza una copia exacta de la base de datos con todos los datos almacenados en ese momento, y exporta un archivo con extensión sql 
                dentro del directorio del proyecto del sistema.
            </p>
            <p>- Luego de hacer la copia de seguridad, hay un select que carga todos los archivos sql que se han generado y al seleccionar podrá realizarse la restauración de la base de datos al punto que el archivo sql tenia en esos momentos.</p>
            <p>OJO: este módulo solo esta disponible para el usuario ADMINISTRADOR, ningún usuario Vendedor tiene acceso a este módulo.</p>
            <br>
                <img src="../img/moduloback.png" border="1" width="300" height="500">    
            <br>
            <a href="home.php"><img title="Volver" src="../img/back.png" width="60" height="60"></a>
        </div>
        
    </body>
</html>