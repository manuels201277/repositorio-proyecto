<?php 

$Producto = new productos();
if((isset($_POST['codigo1'])) && ($_POST['codigo1'] != ''))
{
    $Producto->setidproducto($_POST ['codigo1']);
    $Rest = $Producto->BUSCAR_X_CODIGO();
}
?>
<?php
	if(
		(isset($_POST['codigo2'])) && ($_POST['codigo2'] != '')&&
		(isset($_POST['nombre2'])) && ($_POST['nombre2'] != '')&&
		(isset($_POST['descripcion2'])) && ($_POST['descripcion2'] != '')&&
		(isset($_POST['precio2'])) && ($_POST['precio2'] != '')&&
		(isset($_POST['stock2'])) && ($_POST['stock2'] != '')
		)
	{
		$Producto->setidproducto($_POST ['codigo2']);
		$respuesta=$Producto->Comprobar_Codigo();
		$stock=$_POST['stock2'];
		if($respuesta==true && $stock==0)
		{
			echo "<script> alert('Se Modificara El Stock a 0 y el producto no estare disponible para ventas');</script>";
			$Producto->setnombreproducto($_POST ['nombre2']);  
			$Producto->setstock($_POST ['stock2']); 
			$Producto->setdescripcion($_POST ['descripcion2']);
			$Producto->setprecio($_POST ['precio2']);
			$resul = $Producto->MODIFICAR();
			if($resul == true)
			{	
				echo "<script> alert('Actualizacion Exitosa');
				window.location= ''</script>";
			}
			else
			{
				echo "<script> alert('La Actualizacion a Fallado'); window.location=''</script>";   
			}
		}
		elseif ($respuesta==true ) {
			$Producto->setnombreproducto($_POST ['nombre2']);  
			$Producto->setstock($_POST ['stock2']); 
			$Producto->setdescripcion($_POST ['descripcion2']);
			$Producto->setprecio($_POST ['precio2']);
			$resul = $Producto->MODIFICAR();
			if($resul == true)
			{	
				echo "<script> alert('Actualizacion Exitosa');
				window.location= ''</script>";
			}
			else
			{
				echo "<script> alert('La Actualizacion a Fallado'); window.location=''</script>";   
			}
			
		}
		else
		{
			echo "<script> alert('La Actualizacion a Fallado'); window.location=''</script>";   
		}
	}
	else
	{
			
	}
?>


<!DOCTYPE html>
<html>
    <head>
        <title>Modificar Producto</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
        <link rel="stylesheet" type="text/css" href="../css/EstiloModificar.css">
    </head>
    <body >
		 <div id="Contenedor" class="center">
		 	<div class="Icon">
                <!--Icono de usuario-->
                <img src="../img/product.png" width="50px" height="50px"/>
            </div>
			<div class="ContentForm ">
		 	<form  class="text-center" action="" method="post" name="FormEntrar">
				<label for="inputAddress">SELECCIONE EL PRODUCTO</label>
				<div class="input-group input-group-lg center">
					<select   id="combo" name="codigo1" class="form-control"> 
					<?php while ($registro=$resultado->fetch_assoc()) {?>
							<option  value="<?php echo $registro['ID_PRODUCTO'];?>"><?php echo $registro['NOMBRE_PRODUCTO'];
								}?> </option>
					</select><br>
				</div>
				<br>
				<div class=" text-center ">
					<table align="center">
					<tr>
					<td><button class="btn  btn-primary  btn-signin" id="IngresoLog" width="80px" height="80px" type="submit">CARGAR</button></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td> <a class="btn btn-danger" href="../modulos/home.php" role="button">VOLVER</a></td>
					</tr>
					</table>  
				</div>
		 	</form>
			<br>
			 <?php if(isset($_POST['codigo1'])){  ?>
            <form action="" method="post" name="FormEntrar2"  style=" text-align:center;">
				
              <?php  foreach ($Rest as $Rest) {?>
			  <label class=" text-center "for="inputAddress">Codigo del Producto</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control text-center" name="codigo2"  readonly="true" value="<?php echo $Rest['ID_PRODUCTO'];?>"
				  readonly="true" aria-describedby="sizing-addon1" required>
				</div>
				<br>

				<label for="inputAddress">Nombre del Producto</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control text-center" style = "text-transform:uppercase" name="nombre2" placeholder="ej: Queso" 
				  value="<?php echo $Rest['NOMBRE_PRODUCTO'];?>"
				  aria-describedby="sizing-addon1" required>
				</div>
				<br>
            <label for="inputAddress2">Descripcion</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="descripcion2" class="form-control text-center" style = "text-transform:uppercase" placeholder="ej:producto para vida sana" value="<?php echo $Rest['DESCRIPCION'];?>"
				  aria-describedby="sizing-addon1" required>
				</div>
			<label for="inputAddress2">Stock</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="stock2" class="form-control text-center" placeholder="ingrese cantidad"   maxlength = "10" value="<?php echo $Rest['STOCK'];?>"
				  pattern="^[0-9]+" aria-describedby="sizing-addon1" required>
				</div>
				<br>
                <label for="inputAddress2">Precio</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="precio2" class="form-control text-center" placeholder="ej:5000"  maxlength = "6" value="<?php echo $Rest['PRECIO'];?>"
				  pattern="^[0-9]+" aria-describedby="sizing-addon1" required>
				</div>
				<br>
				<div >
                <table  align="center"> 
					<tr>
						<td>
							<button class="btn  btn-primary  btn-signin " id="IngresoLog" width="80px" 
							height="80px" type="submit">ACTUALIZAR</button>
						</td>
					</tr>
                </table> 
				</div>              
			  <?php } ?>
            </form>
            <?php } ?>   
		 </div>	
		 </div>
</body>
</html>