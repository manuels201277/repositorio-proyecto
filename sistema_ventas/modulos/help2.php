<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Centro de Ayuda</title>
        <link rel="stylesheet" type="text/css" href="../css/estiloayuda.css">
    </head>
    <body>
    <div id="cont">
        <div id="encabezado">
            <h1>Cento de ayuda Vendedor</h1>
        </div>
        <div id="contenido">
            <h2>Bienvenidos <span>al centro de ayuda para el Vendedor</span></h2>
            <p>- Primero que todo, el sistema tiene una funcionalidad en la cual pide autenticación, que consiste en ingresar su RUN y su clave de acceso. </p>
            <br>
            <img src="../img/login.png" border="1" width="300" height="500">
            <br>
            <p>- Luego de iniciar sesión, el sistema lo redireccionará a su menu correspondinte, el cual el usuario vendedor solo posee las funcionalidades de agregar, modificar y eliminar productos, generar ventas y despachos.
            <br>
            <img src="../img/menuvendedor.png" border="1" width="400" height="200">
            <br>
            <p> - Cada uno de los formularios estan validados con los campos que deben ir, es decir, en los campos donde se ingresan textos como nombre, apellido etc. sólo textos en mayúsculas
                y los campos que sean de tipo numérico, solo se deben ingresar digitos.
            </p>
            <p> Cualquier dato que no cumplan el formato arrojará una alerta indicando cual es el error cometido a la hora 
                de rellenar el formulario.
            </p>
            <p>- Si usted presenta problemas con su clave de usuario, ya sea se le olvido o el sistema no le da el acceso es necesario que usted se comunique con el administrador del sistema para que el le de la respectiva solución.</p>
            <a href="home_vendedor.php"><img title="Volver" src="../img/back.png" width="60" height="60"></a>
        </div>
        
    </body>
</html>