<!DOCTYPE html>
<html>
<head>
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<link rel="stylesheet" type="text/css" href="">
<style>
table, th, td {
  border: 2px solid black;
  border-collapse: collapse;
  font-family:verdana;
 
}
th, td {
  padding: 5px;
  text-align: center;
  font-family:verdana;
  font-size:12px;
}
</style>
</head>
<body align="center">
    <div id="cont">
        <div id="encabezado" class="text-center">
		<br>
            <h1>Ver todos los Productos</h1>
		<br>
        </div>
        <div id="contenido" class="text-center">
			<table style="  margin:0 auto; " class="text-center" >
				<tr class="bg-info">
					<td>ID PRODUCTO</td><td>NOMBRE</td>
					<td>DESCRIPCION</td><td>STOCK</td>
					<td>PRECIO</td>
				</tr>
				<?php
					foreach($datos as $datos)
					{ 
						echo "<tr>
						<td>".$datos['ID_PRODUCTO']."</td>
						<td>".$datos['NOMBRE_PRODUCTO']."</td>
						<td>".$datos['DESCRIPCION']."</td>
						<td>".$datos['STOCK']."</td>
						<td>".$datos['PRECIO']."</td>
						</tr>"; 
					} 
				?>
			</table>
			
		</div>
		<br>
		<div class="text-center">	
		
			<a href="../modulos/home.php"><img title="Volver" src="../img/back.png" width="60" height="60"></a>
		</div>
    </body>
</html>