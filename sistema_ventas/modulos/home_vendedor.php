
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Inicio</title>
    <script Language="JavaScript">
    if(history.forward(1)){
    history.replace(history.forward(1));
    }
    </script>
    <link rel="stylesheet" type="text/css" href="../css/estilomenu.css">
  </head>
  <body>
  <div id="header"align="center" >
      <h1>Sistema de Ventas: Perfil Vendedor</h1>
			<ul class="nav">
				<li>
					<img src="../img/home.png" width="60px" height="60px"/>	
					<a href="home.php">Inicio</a>
				</li>
				<li>
					<img src="../img/product.png" width="60px" height="60px"/>
					<a href="">Productos</a>
					<ul>
						<li><a href="crear_producto.php">Crear Producto</a></li>
						<li><a href="../controlador/CargarProducto.php">Modificar Producto</a></li>
						<li><a href="../controlador/EliminarProducto.php">Eliminar Producto</a></li>
						<li><a href="../controlador/cargar_productos.php">Ver Productos</a></li>
						</li>
					</ul>
				</li>
				<li>
					<img src="../img/sale.png" width="60px" height="60px"/>
					<a href="">Ventas</a>
					<ul>
						<li><a href="../controlador/Cargar_CodigosP_Venta.php">Crear Venta</a></li>
						<li><a href="../controlador/datos_ventas.php">Ver Ventas</a></li>
					</ul>
				</li>
        		<li>
					<img src="../img/shipping.png" width="60px" height="60px"/>
					<a href="">Despachos</a>
					<ul>
						<li><a href="../controlador/Cargar_Despacho.php">Crear Despacho</a></li>
						<li><a href="../controlador/Cargar_M_Despacho.php">Actualizar Estado</a></li>
						<li><a href="">Ver Despachos</a></li>
						</li>
					</ul>
				</li>
				<li>
					<img src="../img/sesion.png" width="60px" height="60px"/>
					<a href="../index.php">Salir</a>
				</li>
			</ul>
		</div>
		<div id="ayuda" align="center">
			<h2>¿Necesitas Ayuda?</h2>
			<a href="help2.php"><img title="Ayuda" src="../img/help.png" width="80" height="80"></a>
		</div>
  </body>
</html>