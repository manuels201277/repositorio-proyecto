
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Inicio</title>
    <script Language="JavaScript">
    if(history.forward(1)){
    history.replace(history.forward(1));
    }
    </script>
    <link rel="stylesheet" type="text/css" href="../css/estilomenu.css">
  </head>
  <header>
  <div id="header"align="center" >
      <h1>Sistema de Ventas: Perfil Administrador</h1>
			<ul class="nav">
        		<li>
					<img src="../img/user.png" width="60px" height="60px"/>
					<a href="">Usuarios</a>
					<ul>
						<li><a href="crear_usuario.php">Crear Usuario</a></li>
						<li><a href="../controlador/CargarUsuario.php">Modificar Usuario</a></li>
						<li><a href="../controlador/EliminarUsuario.php">Eliminar Usuario</a></li>
						<li><a href="../controlador/cargar_usuarios.php">Ver Usuarios</a></li>
						</li>
					</ul>
				</li>
				<li>
					<img src="../img/product.png" width="60px" height="60px"/>
					<a href="">Productos</a>
					<ul>
						<li><a href="crear_producto.php">Crear Producto</a></li>
						<li><a href="../controlador/CargarProducto.php">Modificar Producto</a></li>
						<li><a href="../controlador/EliminarProducto.php">Eliminar Producto</a></li>
						<li><a href="../controlador/cargar_productos.php">Ver Productos</a></li>
						</li>
					</ul>
				</li>
				<li>
					<img src="../img/sale.png" width="60px" height="60px"/>
					<a href="">Ventas</a>
					<ul>
						<li><a href="../controlador/Cargar_CodigosP_Venta.php">Crear Venta</a></li>
						<li><a href="../controlador/datos_ventas.php">Ver Ventas</a></li>
					</ul>
				</li>
        		<li>
					<img src="../img/shipping.png" width="60px" height="60px"/>
					<a href="">Despachos</a>
					<ul>
						<li><a href="../controlador/Cargar_Despacho.php">Crear Despacho</a></li>
						<li><a href="../controlador/Cargar_M_Despacho.php">Actualizar Estado</a></li>
						<li><a href="../controlador/datos_despachos.php">Ver Despachos</a></li>
						</li>
					</ul>
				</li>
				<li>
					<img src="../img/registros.png" width="60px" height="60px"/>
					<a href="">Opciones</a>
					<ul>
						<li><a href="../backup/php/modulo_backup.php">Backup</a></li>
						<li><a href="../controlador/Datos_bitacora.php">Ver Bitácora</a></li>
						</li>
					</ul>
				</li>
				<li>
					<img src="../img/sesion.png" width="60px" height="60px"/>
					<a href="../index.php">Salir</a>
				</li>
			</ul>
		</div>
  </header>
  <body>
	<div id="ayuda" align="center">
		<h2>¿Necesitas Ayuda?</h2>
		<a href="help.php"><img title="Ayuda" src="../img/help.png" width="80" height="80"></a>
	</div>
  </body>
</html>