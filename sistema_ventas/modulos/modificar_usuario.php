<?php 
$Usuario = new usuarios();
if((isset($_POST['rut1'])) && ($_POST['rut1'] != ''))
{
    $Usuario->setrut($_POST ['rut1']);
    $Rest = $Usuario->BUSCAR_X_RUT();
}
?>
<?php
	if(
		(isset($_POST['rut2'])) && ($_POST['rut2'] != '')&&
		(isset($_POST['nombre2'])) && ($_POST['nombre2'] != '')&&
		(isset($_POST['apellido2'])) && ($_POST['apellido2'] != '')&&
		(isset($_POST['clave2'])) && ($_POST['clave2'] != '')&&
		(isset($_POST['tipo2'])) && ($_POST['tipo2'] != '')
		)
	{
		$Usuario->setrut($_POST ['rut2']);
		$respuesta=$Usuario->Comprobar_Rut();
		if ($respuesta==true ) 
		{
			$Usuario->setnombre($_POST ['nombre2']);  
			$Usuario->setapellido($_POST ['apellido2']); 
			$Usuario->setclave($_POST ['clave2']);
			$Usuario->settipo($_POST ['tipo2']);
			$resul = $Usuario->modificar();
			if($resul == true)
			{	
				echo "<script> alert('Actualizacion Exitosa');
				window.location= ''</script>";
			}
			else
			{
				echo "<script> alert('La Actualizacion a Fallado'); window.location=''</script>";   
			}
			
		}
		else
		{
			echo "<script> alert('La Actualizacion a Fallado 1'); window.location=''</script>";   
		}
	}
	else
	{
		
	}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Modificar Usuario</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
        <link rel="stylesheet" type="text/css" href="../css/ModificaUsuario.css">
    </head>
    <body >
		 <div id="Contenedor" class="center">
		 	<div class="Icon">
                <!--Icono de usuario-->
                <img src="../img/user.png" width="50px" height="50px"/>
            </div>
			<div class="ContentForm ">
		 	<form  class="text-center" action="" method="post" name="FormEntrar">
			<label for="inputAddress" id="ruts">RUN DEL USUARIO</label>
				<div class="input-group input-group-lg center">
					<select  id="combo" name="rut1" class="form-control"> 
					<?php while ($registro=$resultado->fetch_assoc()) {?>
						<option  value="<?php echo $registro['RUT'];?>"><?php echo $registro['RUT'];
							}?> </option>
					</select><br>
				</div>
				<br>
				<div class=" text-center ">
					<table align="center">
					<tr>
					<td><button class="btn  btn-primary  btn-signin" id="IngresoLog" width="80px" height="80px" type="submit">CARGAR</button></td>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td> <a class="btn btn-danger" href="../modulos/home.php" role="button">VOLVER</a></td>
					</tr>
					</table>  
				</div>
		 	</form>
			<br>
			 <?php if(isset($_POST['rut1'])){  ?>
            <form action="" method="post" name="FormEntrar2"  style=" text-align:center;">
				
              <?php  foreach ($Rest as $Rest) {?>
			  <label class=" text-center "for="inputAddress">Run Usuario</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control text-center" name="rut2"  readonly="true" value="<?php echo $Rest['RUT'];?>"
				  readonly="true" aria-describedby="sizing-addon1" required>
				</div>
				<br>

				<label for="inputAddress">Nombre</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control text-center" style = "text-transform:uppercase" name="nombre2" placeholder="ingrese nombre" 
				  value="<?php echo $Rest['NOMBRE'];?>"
				  aria-describedby="sizing-addon1" required>
				</div>
				<br>
            <label for="inputAddress2">Apellido</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="apellido2" class="form-control text-center" style = "text-transform:uppercase" placeholder="ingrese apellido" value="<?php echo $Rest['APELLIDO'];?>"
				  aria-describedby="sizing-addon1" required>
				</div>
				<br>
			<label for="inputAddress2">Clave</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="clave2" class="form-control text-center" placeholder="ingrese clave"  maxlength = "4" value="<?php echo $Rest['CLAVE'];?>"
				  pattern="^[0-9]+" aria-describedby="sizing-addon1" required>
				</div>
				<br>
				<label for="inputAddress">Tipo usuario</label>
				<div class="input-group input-group-lg center">
					<select  id="combo" name="tipo2"  class="form-control"> 
					<?php 
						if($Rest['TIPO_USUARIO']==1)
						{
							?><option  value="<?php echo $Rest['TIPO_USUARIO'];?>"><?php echo $Rest['TIPO'];?></option>
							<option  value=2>ADMINISTRADOR</option>
						<?php
						}
						 else
						{?>
							<option  value="<?php echo $Rest['TIPO_USUARIO'];?>"><?php echo $Rest['TIPO'];?></option> 
							<option  value=1>VENDEDOR</option>
						<?php
						}?>
					</select><br>
				</div>
				<br>
				<br>
				<div >
                <table  align="center"> 
					<tr>
						<td>
							<button class="btn  btn-primary  btn-signin " id="IngresoLog" width="80px" 
							height="80px" type="submit">ACTUALIZAR</button>
						</td>
					</tr>
                </table> 
				</div>              
			  <?php } ?>
            </form>
            <?php } ?>   
		 </div>	
		 </div>
</body>
</html>