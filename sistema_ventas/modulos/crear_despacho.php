<?php
 $Despachos = new despachos();
	if(
		(isset($_POST['id1'])) && ($_POST['id1'] != '')&&
		(isset($_POST['origen'])) && ($_POST['origen'] != '')&&
		(isset($_POST['destino'])) && ($_POST['destino'] != '')&&
		(isset($_POST['direccion'])) && ($_POST['direccion'] != '')
		)
		{  		
			$Despachos->setidventa($_POST ['id1']);  
			$Despachos->setorigen($_POST ['origen']); 
			$Despachos->setdestino($_POST ['destino']);
			$Despachos->setdireccion($_POST['direccion']);
			
			$consulta = $Despachos->INSERTAR();
			if($consulta==true)
			{
				echo "<script> alert('Despacho Creado correctamente');</script>";    
					
			}
			else
			{
			
				echo "<script> alert('Ocurrio Un Error Intente Nuevamente');window.location='../modulos/home.php'</script>";
			}
		}	
    else
    {
        
        
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Crear Despacho</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <!-- vinculo a bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Temas-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
        <link rel="stylesheet" type="text/css" href="../css/edespacho.css">
	</head>
	<body>

	
	<div id="Contenedor" align="center">
		 <div class="Icon">
                    <!--Icono de usuario-->
                   <img src="../img/package.png" width="50px" height="50px"/>
        </div>
        <div class="ContentForm">
		 	<form action="" method="post" name="FormEntrar" >
			 <label for="inputAddress" id="ruts">VENTAS DISPONIBLES</label>
			    <div class="input-group input-group-lg center">
					<select   id="id1" name="id1" class="form-control center"> 
					<option>-----Seleccione-------</option>
					<?php while ($registro=$resultado->fetch_assoc()) {?>
							
                            <option  value="<?php echo $registro['ID_VENTA'];?>">
							<?php echo $registro['CANTIDAD'];?> &nbsp;
                        <?php  echo $registro['NOMBRE_PRODUCTO'];?> &nbsp; 
                        <?php  echo $registro['NOMBRE_CLIENTE'];?> &nbsp; 
                         <?php  echo $registro['APELLIDO_CLIENTE'];
								}?> </option>
                    </select>
				</div>

				<br>
				<label for="inputAddress2">ENVIO ORIGEN</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="origen" class="form-control" style = "text-transform:uppercase" placeholder="INGRESE CIUDAD ORIGEN" aria-describedby="sizing-addon1" required>
				</div>
				<br>
                <label for="inputAddress">ENVIO DESTINO</label>
		 		<div class="input-group input-group-lg">
				  <input type="text" class="form-control" name="destino" style = "text-transform:uppercase" placeholder="INGRESE CIUDAD DESTINO" aria-describedby="sizing-addon1" required>
				</div>
                <br>
				<label for="inputAddress2">DIRECCION DE ENVIO</label>
				<div class="input-group input-group-lg">
				  <input  type="text" name="direccion" class="form-control" style = "text-transform:uppercase" placeholder="INGRESE DIRECCION DE ENVIO" aria-describedby="sizing-addon1" required>
				</div>
                <br>
                <button class="btn btn-lg btn-primary  btn-signin" id="IngresoLog" width="80px" height="80px" type="submit">CREAR</button>
		 	</form>
		 	<br>
		 	<a class="btn btn-danger" href="../modulos/home.php" role="button">VOLVER</a>
		 </div>	
	</div>
	
</body>
</html>